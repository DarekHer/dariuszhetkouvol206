﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TKoU.ObjectPool;

namespace TKoU.TowerSimulation
{
    [RequireComponent(typeof(IPoolableObject), typeof(IMovable))]
    public class Bullet : ShootableProjectile
    {
        private IMovable movableComponent;

        private void Awake()
        {
            movableComponent = GetComponent<IMovable>();         
        }


        private void OnEnable()
        {
            movableComponent.onTargetReached += OnBulletReachedTarget;
        }

        private void OnDisable()
        {
            movableComponent.onTargetReached -= OnBulletReachedTarget;
        }

        private void OnBulletReachedTarget()
        {
            if(owner.GetTargetHit() != null)
            {
                ObjectPooler.Instance.ReturnToPool(owner.GetTargetHit());
            }
            else
            {
                SimulationController.Instance.SpawnNextTower(transform.position);
            }          
            ObjectPooler.Instance.ReturnToPool(gameObject);
        }
    }
}