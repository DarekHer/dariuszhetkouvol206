﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TKoU.TowerSimulation
{
	public abstract class ShootableProjectile : MonoBehaviour
	{
		protected IShootable owner;

		public void SetOwner(IShootable owner)
		{
			this.owner = owner;
		}
	}
}

