﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TKoU.ObjectPool;
using System;

namespace TKoU.TowerSimulation
{
    [RequireComponent(typeof(IShootable), typeof(IRotateable))]
    public class Tower : MonoBehaviour, IPoolableObject
    {

		#region Serizlized Fields
		[SerializeField] private GameObject bullet;
        [SerializeField] SpriteRenderer[] spriteRenderers;
        [SerializeField] private Color inactiveColor;
        [SerializeField] private Color activeColor;
        [SerializeField] private Vector2 towerRotationAngleRange;
        [SerializeField] private float shootingSpeed;
        [SerializeField] private Vector2 bulletTravelDistanceRange;
        [SerializeField] private int maxRotations;
        [SerializeField] private float initialDeley;
        [SerializeField] private float betweenShotsDeley;
		#endregion

		#region PrivateFields
		private IShootable shooter;
        private IRotateable rotator;
        private int performedRotationsNumber;
        private WaitForSeconds initailWFS;
        private WaitForSeconds betweenShotsWFS;
		#endregion

		#region Unity Callbacks
		private void Awake()
        {
            initailWFS = new WaitForSeconds(initialDeley);
            betweenShotsWFS = new WaitForSeconds(betweenShotsDeley);
            performedRotationsNumber = 0;
            rotator = GetComponent<IRotateable>();
            shooter = GetComponent<IShootable>();
        }
        #endregion

        #region Public Methods
        public void InitilizeTowerSequence()
        {
            StartCoroutine(InitilizeTowerSequenceCo());
        }
        #endregion

        #region Private Methods
        private IEnumerator InitilizeTowerSequenceCo()
        {
            yield return initailWFS;
            SetColor(inactiveColor);
            StartCoroutine(StartTowerSequenceCo());
        }

        private IEnumerator StartTowerSequenceCo()
        {

            SetColor(activeColor);
            while (!IsMaxRotationNumberReached())
            {

                yield return betweenShotsWFS;

                performedRotationsNumber++;

                float angle = GetRadomAngle();
                rotator.PerformRotation(angle);

                float distance = GetRandomBUlletDIstance();
                ShootBullet(distance);
            }
            SetColor(inactiveColor);
        }

        private void SetColor(Color color)
        {
            foreach (var spriteRenderer in spriteRenderers)
            {
                spriteRenderer.color = color;
            }
        }

        private bool IsMaxRotationNumberReached()
        {
            return performedRotationsNumber >= maxRotations;
        }

        public void ResetPerformedRotationsNumber()
        {
            StopAllCoroutines();
            performedRotationsNumber = 0;
            StartCoroutine(StartTowerSequenceCo());

        }

        private void ShootBullet(float distance)
        {
            Vector2 targetPosition = transform.position + shooter.GetShootingDirection() * distance;
            var hitTarget = Physics2D.Raycast(shooter.GetShootinPosition(), shooter.GetShootingDirection(), distance);
            GameObject hitObj = null;
            if (hitTarget)
            {
                if (hitTarget.collider.GetComponent<Tower>() != null)
                {
                    hitObj = hitTarget.collider.gameObject;
                    targetPosition = hitTarget.collider.transform.position;
                }
            }
            shooter.Shoot(bullet, targetPosition, hitObj, shootingSpeed);
        }

        private float GetRandomBUlletDIstance()
        {
            return UnityEngine.Random.Range(bulletTravelDistanceRange.x, bulletTravelDistanceRange.y);
        }

        private float GetRadomAngle()
        {
            return UnityEngine.Random.Range(towerRotationAngleRange.x, towerRotationAngleRange.y);
        }
        #endregion

        #region IPoolable interface implementation
        public GameObject GetGameObjectReference()
        {
            return gameObject;
        }

        public bool IsValidForPooling()
        {
            return !gameObject.activeInHierarchy;
        }

        public void OnSpawnedInPool()
        {
            gameObject.SetActive(false);
        }

        public void OnTakenFromPool()
        {
            gameObject.SetActive(true);
            SimulationController.Instance.AddToActiveTowers(this);
        }

        public void OnReturnedToPool()
        {
            gameObject.SetActive(false);
            SimulationController.Instance.RemoveFromActiveTowers(this);
        }
        #endregion
    }
}