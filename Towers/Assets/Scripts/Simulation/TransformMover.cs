﻿using System;
using System.Collections;
using System.Collections.Generic;
using TKoU.TowerSimulation;
using UnityEngine;
using TKoU.UpdateSystem;

namespace TKoU.TowerSimulation
{
    public class TransformMover : MonoBehaviour, IMovable, IUpdateable
    {
        private Vector3 targetPosition;
        private float speed;
        public Action onTargetReached { get; set; }

        Vector3 direction;

        private void OnEnable()
        {
            UpdateManager.Register(this);
        }

        private void OnDisable()
        {
            UpdateManager.Unregister(this);
        }

        public void Move(float speed)
        {
            transform.Translate(direction * speed * Time.deltaTime, Space.World);
        }

        public void SetMoveData(Vector3 direction, Vector3 targetPosition, float force)
        {
            speed = force;
            this.targetPosition = targetPosition;
            this.direction = direction;
        }

        public void OnUpdate()
        {
            Move(speed);
            if (Mathf.Abs((transform.position.x - targetPosition.x)) <= 0.1 &&
                Mathf.Abs((transform.position.z - targetPosition.z)) <= 0.1)
            {
                onTargetReached?.Invoke();
            }
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }
    }
}
