﻿using System;
using System.Collections;
using System.Collections.Generic;
using TKoU.TowerSimulation;
using UnityEditor;
using UnityEngine;
using TKoU.ObjectPool;

namespace TKoU.TowerSimulation
{
    public class SimplePoolShooter : MonoBehaviour, IShootable
    {
        [SerializeField] private Transform spawnPoint;
        private GameObject hitTarget;

        public Vector3 GetShootingDirection()
        {
            return spawnPoint.transform.up;
        }

        public Vector3 GetShootinPosition()
        {
            return spawnPoint.transform.position;
        }

        public GameObject GetTargetHit()
        {
            return hitTarget;
        }

        public void SetOwner(GameObject projectile)
        {
            projectile.GetComponent<ShootableProjectile>().SetOwner(this);
        }

        public void Shoot(GameObject prefab, Vector3 targetPosition, GameObject hitTarget, float force)
        {
            var obj = ObjectPooler.Instance.GetObjectByReference(prefab);
            obj.transform.position = spawnPoint.transform.position;
            obj.transform.rotation = spawnPoint.transform.rotation;
            this.hitTarget = hitTarget;
            SetOwner(obj);
            var movableComponent = obj.GetComponent<IMovable>();
            if (movableComponent != null)
            {
                movableComponent.SetMoveData(GetShootingDirection(), targetPosition, force);
            }

        }
    }
}