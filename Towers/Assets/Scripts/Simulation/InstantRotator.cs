﻿using System;
using System.Collections;
using System.Collections.Generic;
using TKoU.TowerSimulation;
using UnityEngine;
using TKoU.UpdateSystem;

namespace TKoU.TowerSimulation
{
    public class InstantRotator : MonoBehaviour, IRotateable
    {
        private Transform myTransform;

        public Action onRotationEnded { get; set; }

        private void Awake()
        {
            myTransform = GetComponent<Transform>();
        }


        public void PerformRotation(float angle)
        {
            myTransform.Rotate(Vector3.forward * angle);
            onRotationEnded?.Invoke();
        }

    }

}
