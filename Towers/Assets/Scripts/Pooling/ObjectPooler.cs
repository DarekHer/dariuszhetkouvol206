﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace TKoU.ObjectPool
{
	public class ObjectPooler : MonoBehaviour
	{

		#region Helper Private Classes
		private class Pool
		{
			public Pool(PoolableObjectSetup setup, GameObject parent, ObjectPooler pooler)
			{
				this.setup = setup;
				this.pooler = pooler;
				this.parent = parent;
			}

			private PoolableObjectSetup setup;
			private GameObject parent;
			private ObjectPooler pooler;
			private List<IPoolableObject> spawnedObjects = new List<IPoolableObject>();
			public IPoolableObject GetValidObject()
			{
				foreach (var spawnedObj in spawnedObjects)
				{
					if (spawnedObj.IsValidForPooling())
						return spawnedObj;

				}
				if (setup.autoIncreasePool)
				{
					var newObj = pooler.SpawnSinglePoolObject(setup.prefab, parent);
					return newObj;
				}
				return null;
			}

			public void AddToPool(IPoolableObject poolable)
			{
				spawnedObjects.Add(poolable);
			}
		}

		[Serializable]
		private class PoolableObjectSetup
		{
			public string id;
			public GameObject prefab;
			public int startingPoolSize;
			public bool autoIncreasePool;
		}
		#endregion

		#region Fields
		[SerializeField] private PoolableObjectSetup[] poolObjectSetups;

		private Dictionary<string, Pool> idToPool = new Dictionary<string, Pool>();
		private Dictionary<GameObject, Pool> prefabToPool = new Dictionary<GameObject, Pool>();

		public static ObjectPooler Instance;
		#endregion

		#region Private Methods
		private void Awake()
		{
			Instance = this;
			CreatePools();
		}


		private void OnValidate()
		{
			if (poolObjectSetups.Any(s => s.prefab.GetComponent<IPoolableObject>() == null))
			{
				Debug.LogError("There is no component implementing IPoolableObject interface on one of the prefabs");
			}
		}

		private void CreatePools()
		{
			foreach (var setup in poolObjectSetups)
			{
				CreatePoolFromSetup(setup);
			}
		}

		private void CreatePoolFromSetup(PoolableObjectSetup setup)
		{
			var parent = CreatePoolParentGameobject(setup);
			var pool = new Pool(setup, parent, this);
			prefabToPool.Add(setup.prefab, pool);
			idToPool.Add(setup.id, pool);

			for (int i = 0; i < setup.startingPoolSize; i++)
			{
				var spawnedObj = SpawnSinglePoolObject(setup.prefab, parent);
				pool.AddToPool(spawnedObj);
			}
		}		

		private GameObject CreatePoolParentGameobject(PoolableObjectSetup setup)
		{
			var parent = new GameObject($"Pool: {setup.id}");
			parent.transform.position = Vector3.zero;
			return parent;
		}
		#endregion

		#region Public Methods
		public IPoolableObject SpawnSinglePoolObject(GameObject prefab, GameObject parent)
		{
			var spawnedInstance = Instantiate(prefab, parent.transform);
			var poolableComponent = spawnedInstance.GetComponent<IPoolableObject>();
			if (poolableComponent != null)
			{
				poolableComponent.OnSpawnedInPool();
			}
			return poolableComponent;
		}

		public GameObject GetObjectByReference(GameObject refrenceObject)
		{
			if (prefabToPool.TryGetValue(refrenceObject, out Pool pool))
			{
				var foundObject = pool.GetValidObject();
				if (foundObject != null)
				{
					foundObject.OnTakenFromPool();
					return foundObject.GetGameObjectReference();
				}
				else
				{
					return null;
				}
			}
			else
			{
				Debug.LogError($"There is no pool for object: {refrenceObject.name}");
				return null;
			}
		}

		public GameObject GetObjectById(string id)
		{
			if (idToPool.TryGetValue(id, out Pool pool))
			{
				var foundObject = pool.GetValidObject();
				if (foundObject != null)
				{
					foundObject.OnTakenFromPool();
					return foundObject.GetGameObjectReference();
				}
				else
				{
					return null;
				}
				
			}
			else
			{
				Debug.LogError($"There is no pool with id: {id}");
				return null;
			}
		}

		public void ReturnToPool(GameObject obj)
		{
			var poolableComponent = obj.GetComponent<IPoolableObject>();
			if (poolableComponent != null)
			{
				poolableComponent.OnReturnedToPool();
			}
		}
		#endregion
	}
}

