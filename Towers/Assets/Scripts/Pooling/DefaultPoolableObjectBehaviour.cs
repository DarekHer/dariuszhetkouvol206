﻿using System.Collections;
using System.Collections.Generic;
using TKoU.ObjectPool;
using UnityEngine;

public class DefaultPoolableObjectBehaviour : MonoBehaviour, IPoolableObject
{

	public GameObject GetGameObjectReference()
	{
		return gameObject; 
	}

	public bool IsValidForPooling()
	{
		return !gameObject.activeInHierarchy;
	}

	public void OnReturnedToPool()
	{
		gameObject.SetActive(false);
	}

	public void OnSpawnedInPool()
	{
		gameObject.SetActive(false);

	}

	public void OnTakenFromPool()
	{
		gameObject.SetActive(true);

	}
}
