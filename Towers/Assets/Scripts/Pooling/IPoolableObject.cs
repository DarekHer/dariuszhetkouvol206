﻿using UnityEngine;

namespace TKoU.ObjectPool
{
	public interface IPoolableObject
	{
		GameObject GetGameObjectReference();
		bool IsValidForPooling();
		void OnSpawnedInPool();
		void OnTakenFromPool();
		void OnReturnedToPool();
	}
}