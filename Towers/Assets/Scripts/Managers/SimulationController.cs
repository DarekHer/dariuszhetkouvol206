﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TKoU.ObjectPool;
using UnityEngine.UI;

namespace TKoU.TowerSimulation
{
	public class SimulationController : MonoBehaviour
	{
		public static SimulationController Instance;

		#region Serizlized Fields
		[Header("Simulation Data")]
		[SerializeField] private int maxTowersNumber;
		
		[Header("Prefabs")]
		[SerializeField] private Tower towerPrefab;

		[Header("UI")]
		[SerializeField] private Text towersNumberText;
		#endregion

		#region Private Fields
		private Vector3 startingPosition = Vector3.zero;

		private List<Tower> spawnedTowers = new List<Tower>();

		private bool isSpawningTowersEnabled;
		#endregion

		#region unity Callbacks

		private void Awake()
		{
			Instance = this;
		}


		private void Start()
		{
			isSpawningTowersEnabled = true;
			StartSimulation();
		}
		#endregion

		#region Public Mtehods
		public void SpawnNextTower(Vector3 position)
		{
			if (!isSpawningTowersEnabled)
				return;
			var tower = ObjectPooler.Instance.GetObjectByReference(towerPrefab.gameObject);

			if (tower != null)
			{
				tower.transform.position = position;
				tower.GetComponent<Tower>().InitilizeTowerSequence();
			}
			if (IsMaxTowersNumberIsReached() && isSpawningTowersEnabled)
				ResetAllTowersRotations();
		}
		public bool IsMaxTowersNumberIsReached()
		{
			return spawnedTowers.Count >= maxTowersNumber;

		}

		public void AddToActiveTowers(Tower tower)
		{
			spawnedTowers.Add(tower);
			Updatetext();

		}
		public void RemoveFromActiveTowers(Tower tower)
		{
			spawnedTowers.Remove(tower);
			Updatetext();
		}
		#endregion

		#region Private Methods
		private void StartSimulation()
		{
			SpawnNextTower(startingPosition);
		}

		private void ResetAllTowersRotations()
		{
			isSpawningTowersEnabled = false;
			foreach (var tower in spawnedTowers)
			{
				tower.ResetPerformedRotationsNumber();
			}
		}

		private void Updatetext()
		{
			towersNumberText.text = $"Towers; {spawnedTowers.Count}";
		}
		#endregion
	}
}