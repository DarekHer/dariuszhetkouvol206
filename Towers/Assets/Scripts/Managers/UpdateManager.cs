﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TKoU.UpdateSystem
{
    public class UpdateManager : MonoBehaviour
    {
        private static List<IUpdateable> updateableObjects = new List<IUpdateable>();

        public static void Register(IUpdateable updatable)
        {
            updateableObjects.Add(updatable);
        }
        public static void Unregister(IUpdateable updatable)
        {
            updateableObjects.Remove(updatable);

        }

        private void Update()
        {
            for (int i = 0; i < updateableObjects.Count; i++)
            {
                updateableObjects[i].OnUpdate();
            }
        }
    }
}

