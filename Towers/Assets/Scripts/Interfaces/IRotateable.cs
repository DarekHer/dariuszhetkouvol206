﻿using System;

namespace TKoU.TowerSimulation
{
	internal interface IRotateable
	{
		void PerformRotation(float angle);
	}
}