﻿using System;
using UnityEngine;

namespace TKoU.TowerSimulation
{
	public interface IShootable
	{
		Vector3 GetShootingDirection();
		GameObject GetTargetHit();
		Vector3 GetShootinPosition();
		void SetOwner(GameObject projectile);
		void Shoot(GameObject prefab, Vector3 targetPosition, GameObject hitTarget, float force);
	}
}