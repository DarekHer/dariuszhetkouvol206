﻿using System;
using UnityEngine;

namespace TKoU.TowerSimulation
{
	public interface IMovable
	{
		Action onTargetReached { get; set; }

		void SetMoveData(Vector3 direction, Vector3 targetPosition, float force);
		void Move(float speed);
		Vector3 GetPosition();
	}
}