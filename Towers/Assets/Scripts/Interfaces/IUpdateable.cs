﻿namespace TKoU.UpdateSystem
{
	public interface IUpdateable
	{
		void OnUpdate();
	}
}

