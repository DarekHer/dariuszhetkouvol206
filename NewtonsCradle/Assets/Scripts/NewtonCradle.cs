﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TKoU.NewtonsCradle
{
    public class NewtonCradle : MonoBehaviour
    {
        [SerializeField] private GameObject ballPrefab;
        [SerializeField] private int ballsNumber;
        [SerializeField] private Vector3 startingPosition;
        [SerializeField] private Vector3 distanceBetweenBalls;

        private void Awake()
        {
            CreateBalls();
        }

        private void CreateBalls()
        {
            for (int i = 0; i < ballsNumber; i++)
            {
                var position = startingPosition + distanceBetweenBalls * i;
                var ballInstance = Instantiate(ballPrefab, position, Quaternion.identity);
                ballInstance.transform.SetParent(transform, true);            
            }
        }
    }
}
