﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TKoU.NewtonsCradle
{
    public class MouseDragger : MonoBehaviour
    {
        private Rigidbody2D rigidbody;
        private Camera mainCamera;

        private void Awake()
        {
            mainCamera = Camera.main;
            rigidbody = GetComponent<Rigidbody2D>();
        }

        private void OnMouseDrag()
        {
            var targetPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            targetPosition = new Vector3(targetPosition.x, targetPosition.y, transform.position.z);
            rigidbody.position = targetPosition;
        }
    }
}

